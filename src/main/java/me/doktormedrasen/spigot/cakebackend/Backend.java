package me.doktormedrasen.spigot.cakebackend;

import lombok.Getter;
import me.doktormedrasen.spigot.cakebackend.api.manager.CakeManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Class by DoktorMedRasen :P
 */

public class Backend extends JavaPlugin {

    @Getter
    private static Backend instance;

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        new CakeManager();
    }

    @Override
    public void onDisable() {
        CakeManager.getInstance().disable();
    }

}
