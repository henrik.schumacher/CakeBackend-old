package me.doktormedrasen.spigot.cakebackend.utils;

import me.doktormedrasen.spigot.cakebackend.api.CakePlayer;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * Class by DoktorMedRasen :P
 */

public class ItemStackUtils {

    public static ItemStack LEATHER_HELMET = new ItemStack(Material.LEATHER_HELMET);
    public static ItemStack LEATHER_CHESTPLATE = new ItemStack(Material.LEATHER_CHESTPLATE);
    public static ItemStack LEATHER_LEGGINGS = new ItemStack(Material.LEATHER_LEGGINGS);
    public static ItemStack LEATHER_BOOTS = new ItemStack(Material.LEATHER_BOOTS);
    public static ItemStack GOLD_HELMET = new ItemStack(Material.GOLD_HELMET);
    public static ItemStack GOLD_CHESTPLATE = new ItemStack(Material.GOLD_CHESTPLATE);
    public static ItemStack GOLD_LEGGINGS = new ItemStack(Material.GOLD_LEGGINGS);
    public static ItemStack GOLD_BOOTS = new ItemStack(Material.GOLD_BOOTS);
    public static ItemStack CHAINMAIL_HELMET = new ItemStack(Material.CHAINMAIL_HELMET);
    public static ItemStack CHAINMAIL_CHESTPLATE = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
    public static ItemStack CHAINMAIL_LEGGINGS = new ItemStack(Material.CHAINMAIL_LEGGINGS);
    public static ItemStack CHAINMAIL_BOOTS = new ItemStack(Material.CHAINMAIL_BOOTS);
    public static ItemStack IRON_HELMET = new ItemStack(Material.IRON_HELMET);
    public static ItemStack IRON_CHESTPLATE = new ItemStack(Material.IRON_CHESTPLATE);
    public static ItemStack IRON_LEGGINGS = new ItemStack(Material.IRON_LEGGINGS);
    public static ItemStack IRON_BOOTS = new ItemStack(Material.IRON_BOOTS);
    public static ItemStack DIAMOND_HELMET = new ItemStack(Material.DIAMOND_HELMET);
    public static ItemStack DIAMOND_CHESTPLATE = new ItemStack(Material.DIAMOND_CHESTPLATE);
    public static ItemStack DIAMOND_LEGGINGS = new ItemStack(Material.DIAMOND_LEGGINGS);
    public static ItemStack DIAMOND_BOOTS = new ItemStack(Material.DIAMOND_BOOTS);
    public static ItemStack SHIELD;
    public static ItemStack ELYTRA;
    public static ItemStack SPECTRAL_ARROW;
    public static ItemStack TIPPED_ARROW;
    public static ItemStack BOW;
    public static ItemStack ARROW;
    public static ItemStack FISHING_ROD;
    public static ItemStack WOOD_AXE;
    public static ItemStack WOOD_SWORD;
    public static ItemStack STONE_AXE;
    public static ItemStack STONE_SWORD;
    public static ItemStack IRON_AXE;
    public static ItemStack IRON_SWORD;
    public static ItemStack GOLD_AXE;
    public static ItemStack GOLD_SWORD;
    public static ItemStack DIAMOND_AXE;
    public static ItemStack DIAMOND_SWORD;
    public static ItemStack ENDER_PEARL;
    public static ItemStack GOLDEN_APPLE;
    public static ItemStack GOD_APPLE;
    public static ItemStack MILK_BUCKET;
    public static ItemStack APPLE;
    public static ItemStack BAKED_POTATO;
    public static ItemStack BREAD;
    public static ItemStack COOKED_BEEF;
    public static ItemStack COOKED_CHICKEN;
    public static ItemStack COOKED_FISH;
    public static ItemStack COOKIE;
    public static ItemStack GRILLED_PORK;
    public static ItemStack MELON;
    public static ItemStack PUMPKIN_PIE;
    public static ItemStack GOLDEN_CARROT;
    public static ItemStack MUSHROOM_SOUP;
    public static ItemStack EMPTY_ITEM;
    public static ItemStack POTION;
    public static ItemStack REGENERATION_POTION;
    public static ItemStack SWIFTNESS_POTION;
    public static ItemStack FIRE_RESISTANCE_POTION;
    public static ItemStack POISON_POTION;
    public static ItemStack HEALING_POTION;
    public static ItemStack NIGHT_VISION_POTION;
    public static ItemStack WEAKNESS_POTION;
    public static ItemStack STRENGTH_POTION;
    public static ItemStack LEAPING_POTION;
    public static ItemStack SLOWNESS_POTION;
    public static ItemStack HARMING_POTION;
    public static ItemStack WATER_BREATHING_POTION;
    public static ItemStack INVISIBILITY_POTION;
    public static ItemStack REGENERATION_POTION_II;
    public static ItemStack SWIFTNESS_POTION_II;
    public static ItemStack POISON_POTION_II;
    public static ItemStack HEALING_POTION_II;
    public static ItemStack STRENGTH_POTION_II;
    public static ItemStack LEAPING_POTION_II;
    public static ItemStack HARMING_POTION_II;
    public static ItemStack REGENERATION_POTION_EXT;
    public static ItemStack SWIFTNESS_POTION_EXT;
    public static ItemStack FIRE_RESISTANCE_POTION_EXT;
    public static ItemStack POISON_POTION_EXT;
    public static ItemStack NIGHT_VISION_POTION_EXT;
    public static ItemStack WEAKNESS_POTION_EXT;
    public static ItemStack STRENGTH_POTION_EXT;
    public static ItemStack SLOWNESS_POTION_EXT;
    public static ItemStack LEAPING_POTION_EXT;
    public static ItemStack WATER_BREATHING_POTION_EXT;
    public static ItemStack INVISIBILITY_POTION_EXT;
    public static ItemStack REGENERATION_POTION_II_EXT;
    public static ItemStack SWIFTNESS_POTION_II_EXT;
    public static ItemStack POISON_POTION_II_EXT;
    public static ItemStack STRENGTH_POTION_II_EXT;
    public static ItemStack REGENERATION_SPLASH;
    public static ItemStack SWIFTNESS_SPLASH;
    public static ItemStack FIRE_RESISTANCE_SPLASH;
    public static ItemStack POISON_SPLASH;
    public static ItemStack HEALING_SPLASH;
    public static ItemStack NIGHT_VISION_SPLASH;
    public static ItemStack WEAKNESS_SPLASH;
    public static ItemStack STRENGTH_SPLASH;
    public static ItemStack SLOWNESS_SPLASH;
    public static ItemStack HARMING_SPLASH;
    public static ItemStack BREATHING_SPLASH;
    public static ItemStack INVISIBILITY_SPLASH;
    public static ItemStack REGENERATION_SPLASH_II;
    public static ItemStack SWIFTNESS_SPLASH_II;
    public static ItemStack POISON_SPLASH_II;
    public static ItemStack HEALING_SPLASH_II;
    public static ItemStack STRENGTH_SPLASH_II;
    public static ItemStack LEAPING_SPLASH_II;
    public static ItemStack HARMING_SPLASH_II;
    public static ItemStack REGENERATION_SPLASH_EXT;
    public static ItemStack SWIFTNESS_SPLASH_EXT;
    public static ItemStack FIRE_RESISTANCE_SPLASH_EXT;
    public static ItemStack POISON_SPLASH_EXT;
    public static ItemStack NIGHT_VISION_SPLASH_EXT;
    public static ItemStack WEAKNESS_SPLASH_EXT;
    public static ItemStack STRENGTH_SPLASH_EXT;
    public static ItemStack SLOWNESS_SPLASH_EXT;
    public static ItemStack LEAPING_SPLASH_EXT;
    public static ItemStack BREATHING_SPLASH_EXT;
    public static ItemStack INVISIBILITY_SPLASH_EXT;
    public static ItemStack REGENERATION_SPLASH_II_EXT;
    public static ItemStack POISON_SPLASH_II_EXT;
    public static ItemStack STRENGTH_SPLASH_II_EXT;

    public static ItemStack[] LEATHER_ARMOR() {
        return new ItemStack[]{LEATHER_BOOTS, LEATHER_LEGGINGS, LEATHER_CHESTPLATE, LEATHER_HELMET};
    }

    public static ItemStack[] LEATHER_ARMOR_COLORED(Color color) {
        ItemStack[] itemStacks;
        for (ItemStack itemStack : itemStacks = ItemStackUtils.LEATHER_ARMOR()) {
            LeatherArmorMeta meta = (LeatherArmorMeta)itemStack.getItemMeta();
            meta.setColor(color);
            itemStack.setItemMeta((ItemMeta) meta);
        }
        return itemStacks;
    }

    public static ItemStack[] GOLD_ARMOR() {
        return new ItemStack[]{GOLD_BOOTS, GOLD_LEGGINGS, GOLD_CHESTPLATE, GOLD_HELMET};
    }

    public static ItemStack[] CHAINMAIL_ARMOR() {
        return new ItemStack[]{CHAINMAIL_BOOTS, CHAINMAIL_LEGGINGS, CHAINMAIL_CHESTPLATE, CHAINMAIL_HELMET};
    }

    public static ItemStack[] IRON_ARMOR() {
        return new ItemStack[]{IRON_BOOTS, IRON_LEGGINGS, IRON_CHESTPLATE, IRON_HELMET};
    }

    public static ItemStack[] DIAMOND_ARMOR() {
        return new ItemStack[]{DIAMOND_BOOTS, DIAMOND_LEGGINGS, DIAMOND_CHESTPLATE, DIAMOND_HELMET};
    }

    public static ItemStack[] ALL_ARMOR() {
        return new ItemStack[]{LEATHER_HELMET, LEATHER_CHESTPLATE, LEATHER_LEGGINGS, LEATHER_BOOTS, CHAINMAIL_HELMET, CHAINMAIL_CHESTPLATE, CHAINMAIL_LEGGINGS, CHAINMAIL_BOOTS, IRON_HELMET, IRON_CHESTPLATE, IRON_LEGGINGS, IRON_BOOTS, GOLD_HELMET, GOLD_CHESTPLATE, GOLD_LEGGINGS, GOLD_BOOTS, DIAMOND_HELMET, DIAMOND_CHESTPLATE, DIAMOND_LEGGINGS, DIAMOND_BOOTS};
    }

    public static ItemStack[] WOOD_WEAPONS() {
        return new ItemStack[]{WOOD_AXE, WOOD_SWORD};
    }

    public static ItemStack[] STONE_WEAPONS() {
        return new ItemStack[]{STONE_AXE, STONE_SWORD};
    }

    public static ItemStack[] IRON_WEAPONS() {
        return new ItemStack[]{IRON_AXE, IRON_SWORD};
    }

    public static ItemStack[] GOLD_WEAPONS() {
        return new ItemStack[]{GOLD_AXE, GOLD_SWORD};
    }

    public static ItemStack[] DIAMOND_WEAPONS() {
        return new ItemStack[]{DIAMOND_AXE, DIAMOND_SWORD};
    }

    public static ItemStack[] ALL_WEAPONS() {
        return new ItemStack[]{BOW, ARROW, WOOD_AXE, WOOD_SWORD, STONE_AXE, STONE_SWORD, IRON_AXE, IRON_SWORD, GOLD_AXE, GOLD_SWORD, DIAMOND_AXE, DIAMOND_SWORD, FISHING_ROD};
    }

    public static ItemStack[] ALL_FOOD() {
        return new ItemStack[]{APPLE, BAKED_POTATO, BREAD, COOKED_BEEF, COOKED_CHICKEN, COOKED_FISH, COOKIE, GRILLED_PORK, MELON, PUMPKIN_PIE, GOLDEN_CARROT, MUSHROOM_SOUP};
    }

    public static ItemStack addUnbreaking(ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.spigot().setUnbreakable(true);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static void addUnbreakingToArmor(CakePlayer player) {
        for (ItemStack itemStack : player.getInventory().getArmorContents()) {
            if (itemStack == null || itemStack.getType() == Material.AIR) continue;
            ItemStackUtils.addUnbreaking(itemStack);
        }
    }

    public static void addUnbreakingToWeapons(CakePlayer player) {
        for (ItemStack itemStack : player.getInventory().getContents()) {
            Material type;
            if (itemStack == null || itemStack.getType() == Material.AIR || (type = itemStack.getType()) != Material.WOOD_SWORD && type != Material.STONE_SWORD && type != Material.GOLD_SWORD && type != Material.IRON_SWORD && type != Material.DIAMOND_SWORD && type != Material.BOW) continue;
            ItemStackUtils.addUnbreaking(itemStack);
        }
    }

    public static ItemStack removeUnbreaking(ItemStack itemStack) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.spigot().setUnbreakable(false);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static void removeUnbreakingFromArmor(CakePlayer player) {
        for (ItemStack itemStack : player.getInventory().getArmorContents()) {
            if (itemStack == null || itemStack.getType() == Material.AIR) continue;
            ItemStackUtils.removeUnbreaking(itemStack);
        }
    }

    public static void removeUnbreakingFromWeapons(CakePlayer player) {
        for (ItemStack itemStack : player.getInventory().getContents()) {
            Material type;
            if (itemStack == null || itemStack.getType() == Material.AIR || (type = itemStack.getType()) != Material.WOOD_SWORD && type != Material.STONE_SWORD && type != Material.GOLD_SWORD && type != Material.IRON_SWORD && type != Material.DIAMOND_SWORD && type != Material.BOW) continue;
            ItemStackUtils.removeUnbreaking(itemStack);
        }
    }

    static {
        BOW = new ItemStack(Material.BOW);
        ARROW = new ItemStack(Material.ARROW);
        FISHING_ROD = new ItemStack(Material.FISHING_ROD);
        WOOD_AXE = new ItemStack(Material.WOOD_AXE);
        WOOD_SWORD = new ItemStack(Material.WOOD_SWORD);
        STONE_AXE = new ItemStack(Material.STONE_AXE);
        STONE_SWORD = new ItemStack(Material.STONE_SWORD);
        IRON_AXE = new ItemStack(Material.IRON_AXE);
        IRON_SWORD = new ItemStack(Material.IRON_SWORD);
        GOLD_AXE = new ItemStack(Material.GOLD_AXE);
        GOLD_SWORD = new ItemStack(Material.GOLD_SWORD);
        DIAMOND_AXE = new ItemStack(Material.DIAMOND_AXE);
        DIAMOND_SWORD = new ItemStack(Material.DIAMOND_SWORD);
        ENDER_PEARL = new ItemStack(Material.ENDER_PEARL);
        GOLDEN_APPLE = new ItemStack(Material.GOLDEN_APPLE);
        GOD_APPLE = new ItemStack(Material.GOLDEN_APPLE, 1, (short) 1);
        MILK_BUCKET = new ItemStack(Material.MILK_BUCKET);
        APPLE = new ItemStack(Material.APPLE);
        BAKED_POTATO = new ItemStack(Material.BAKED_POTATO);
        BREAD = new ItemStack(Material.BREAD);
        COOKED_BEEF = new ItemStack(Material.COOKED_BEEF);
        COOKED_CHICKEN = new ItemStack(Material.COOKED_CHICKEN);
        COOKED_FISH = new ItemStack(Material.COOKED_FISH);
        COOKIE = new ItemStack(Material.COOKIE);
        GRILLED_PORK = new ItemStack(Material.GRILLED_PORK);
        MELON = new ItemStack(Material.MELON);
        PUMPKIN_PIE = new ItemStack(Material.PUMPKIN_PIE);
        GOLDEN_CARROT = new ItemStack(Material.GOLDEN_CARROT);
        MUSHROOM_SOUP = new ItemStack(Material.MUSHROOM_SOUP);
        EMPTY_ITEM = new ItemStack(Material.AIR);
        POTION = new ItemStack(Material.POTION);
        REGENERATION_POTION = new ItemStack(Material.POTION, 1, (short) 8193);
        SWIFTNESS_POTION = new ItemStack(Material.POTION, 1, (short) 8194);
        FIRE_RESISTANCE_POTION = new ItemStack(Material.POTION, 1, (short) 8195);
        POISON_POTION = new ItemStack(Material.POTION, 1, (short) 8196);
        HEALING_POTION = new ItemStack(Material.POTION, 1, (short) 8197);
        NIGHT_VISION_POTION = new ItemStack(Material.POTION, 1, (short) 8198);
        WEAKNESS_POTION = new ItemStack(Material.POTION, 1, (short) 8200);
        STRENGTH_POTION = new ItemStack(Material.POTION, 1, (short) 8201);
        LEAPING_POTION = new ItemStack(Material.POTION, 1, (short) 8202);
        SLOWNESS_POTION = new ItemStack(Material.POTION, 1, (short) 8203);
        HARMING_POTION = new ItemStack(Material.POTION, 1, (short) 8204);
        WATER_BREATHING_POTION = new ItemStack(Material.POTION, 1, (short) 8205);
        INVISIBILITY_POTION = new ItemStack(Material.POTION, 1, (short) 8206);
        REGENERATION_POTION_II = new ItemStack(Material.POTION, 1, (short) 8225);
        SWIFTNESS_POTION_II = new ItemStack(Material.POTION, 1, (short) 8226);
        POISON_POTION_II = new ItemStack(Material.POTION, 1, (short) 8228);
        HEALING_POTION_II = new ItemStack(Material.POTION, 1, (short) 8229);
        STRENGTH_POTION_II = new ItemStack(Material.POTION, 1, (short) 8233);
        LEAPING_POTION_II = new ItemStack(Material.POTION, 1, (short) 8235);
        HARMING_POTION_II = new ItemStack(Material.POTION, 1, (short) 8236);
        REGENERATION_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8257);
        SWIFTNESS_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8258);
        FIRE_RESISTANCE_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8259);
        POISON_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8260);
        NIGHT_VISION_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8262);
        WEAKNESS_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8264);
        STRENGTH_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8265);
        SLOWNESS_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8266);
        LEAPING_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8267);
        WATER_BREATHING_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8269);
        INVISIBILITY_POTION_EXT = new ItemStack(Material.POTION, 1, (short) 8270);
        REGENERATION_POTION_II_EXT = new ItemStack(Material.POTION, 1, (short) 8289);
        SWIFTNESS_POTION_II_EXT = new ItemStack(Material.POTION, 1, (short) 8290);
        POISON_POTION_II_EXT = new ItemStack(Material.POTION, 1, (short) 8292);
        STRENGTH_POTION_II_EXT = new ItemStack(Material.POTION, 1, (short) 8297);
        REGENERATION_SPLASH = new ItemStack(Material.POTION, 1, (short) 16385);
        SWIFTNESS_SPLASH = new ItemStack(Material.POTION, 1, (short) 16386);
        FIRE_RESISTANCE_SPLASH = new ItemStack(Material.POTION, 1, (short) 16387);
        POISON_SPLASH = new ItemStack(Material.POTION, 1, (short) 16388);
        HEALING_SPLASH = new ItemStack(Material.POTION, 1, (short) 16389);
        NIGHT_VISION_SPLASH = new ItemStack(Material.POTION, 1, (short) 16390);
        WEAKNESS_SPLASH = new ItemStack(Material.POTION, 1, (short) 16392);
        STRENGTH_SPLASH = new ItemStack(Material.POTION, 1, (short) 16393);
        SLOWNESS_SPLASH = new ItemStack(Material.POTION, 1, (short) 16394);
        HARMING_SPLASH = new ItemStack(Material.POTION, 1, (short) 16396);
        BREATHING_SPLASH = new ItemStack(Material.POTION, 1, (short) 16397);
        INVISIBILITY_SPLASH = new ItemStack(Material.POTION, 1, (short) 16398);
        REGENERATION_SPLASH_II = new ItemStack(Material.POTION, 1, (short) 16417);
        SWIFTNESS_SPLASH_II = new ItemStack(Material.POTION, 1, (short) 16418);
        POISON_SPLASH_II = new ItemStack(Material.POTION, 1, (short) 16420);
        HEALING_SPLASH_II = new ItemStack(Material.POTION, 1, (short) 16421);
        STRENGTH_SPLASH_II = new ItemStack(Material.POTION, 1, (short) 16425);
        LEAPING_SPLASH_II = new ItemStack(Material.POTION, 1, (short) 16427);
        HARMING_SPLASH_II = new ItemStack(Material.POTION, 1, (short) 16428);
        REGENERATION_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16449);
        SWIFTNESS_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16450);
        FIRE_RESISTANCE_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16451);
        POISON_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16452);
        NIGHT_VISION_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16454);
        WEAKNESS_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16456);
        STRENGTH_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16457);
        SLOWNESS_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16458);
        LEAPING_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16459);
        BREATHING_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16461);
        INVISIBILITY_SPLASH_EXT = new ItemStack(Material.POTION, 1, (short) 16481);
        REGENERATION_SPLASH_II_EXT = new ItemStack(Material.POTION, 1, (short) 16482);
        POISON_SPLASH_II_EXT = new ItemStack(Material.POTION, 1, (short) 16484);
        STRENGTH_SPLASH_II_EXT = new ItemStack(Material.POTION, 1, (short) 16489);
    }

}
