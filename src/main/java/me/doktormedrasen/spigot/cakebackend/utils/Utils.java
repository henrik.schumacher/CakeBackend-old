package me.doktormedrasen.spigot.cakebackend.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Bukkit;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class Utils {

    public static UUID fetchUUID(String username) {
        try {
            URL url_0 = new URL("https://api.mojang.com/users/profiles/minecraft/" + username);
            InputStreamReader reader_0 = new InputStreamReader(url_0.openStream());
            return getUUID(new JsonParser().parse(reader_0).getAsJsonObject().get("id").getAsString());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static UUID getUUID(String id) {
        return UUID.fromString(id.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
    }

    public static GameProfile buildProfile(String texture) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", texture));
        return profile;
    }

    public static String getPlayerTextures(String name) {
        try {
            URL url_0 = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
            InputStreamReader reader_0 = new InputStreamReader(url_0.openStream());
            String uuid = new JsonParser().parse(reader_0).getAsJsonObject().get("id").getAsString();

            URL url_1 = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
            InputStreamReader reader_1 = new InputStreamReader(url_1.openStream());
            JsonObject textureProperty = new JsonParser().parse(reader_1).getAsJsonObject().get("properties").getAsJsonArray().get(0).getAsJsonObject();

            return textureProperty.get("value").getAsString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getPlayerTextures(UUID uniqueId) {
        try {
            URL url_0 = new URL("https://api.mojang.com/users/profiles/minecraft/" + Bukkit.getOfflinePlayer(uniqueId).getName());
            InputStreamReader reader_0 = new InputStreamReader(url_0.openStream());
            String uuid = new JsonParser().parse(reader_0).getAsJsonObject().get("id").getAsString();

            URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
            InputStreamReader reader = new InputStreamReader(url.openStream());
            JsonObject textureProperty = new JsonParser().parse(reader).getAsJsonObject().get("properties").getAsJsonArray().get(0).getAsJsonObject();

            return textureProperty.get("value").getAsString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getCraftbukkitVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    }

    public static Class getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + Utils.getCraftbukkitVersion() + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
