package me.doktormedrasen.spigot.cakebackend.api;

import lombok.Getter;
import me.doktormedrasen.spigot.cakebackend.api.inventory.CakeInventory;
import me.doktormedrasen.spigot.cakebackend.api.inventory.CakeItem;
import me.doktormedrasen.spigot.cakebackend.api.manager.CakeManager;
import me.doktormedrasen.spigot.cakebackend.api.scoreboard.CakeScoreboard;
import me.doktormedrasen.spigot.cakebackend.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scoreboard.Scoreboard;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 **/

@Getter
public class CakePlayer extends PlayerData {

    private UUID uniqueId;

    public CakePlayer(UUID uniqueId) {
        super(uniqueId);
        this.uniqueId = uniqueId;
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(this.uniqueId);
    }

    public GameMode getGameMode() {
        return this.getBukkitPlayer().getGameMode();
    }

    public void setGameMode(GameMode gameMode) {
        this.getBukkitPlayer().setGameMode(gameMode);
    }

    public double getHealth() {
        return this.getBukkitPlayer().getHealth();
    }

    public void setHealth(double health) {
        this.getBukkitPlayer().setHealth(health);
    }

    public int getFoodLevel() {
        return this.getBukkitPlayer().getFoodLevel();
    }

    public void setFoodLevel(int foodLevel) {
        this.getBukkitPlayer().setFoodLevel(foodLevel);
    }


    public float getExp() {
        return this.getBukkitPlayer().getExp();
    }

    public void setExp(float exp) {
        this.getBukkitPlayer().setExp(exp);
    }

    public int getLevel() {
        return this.getBukkitPlayer().getLevel();
    }

    public void setLevel(int level) {
        this.getBukkitPlayer().setLevel(level);
    }

    public String getDisplayname() {
        return this.getBukkitPlayer().getDisplayName();
    }

    public void setDisplayname(String displayname) {
        this.getBukkitPlayer().setDisplayName(displayname);
    }

    public CakeScoreboard getScoreboard() {
        return new CakeScoreboard(this);
    }

    public void setScoreboard(CakeScoreboard cakeScoreboard) {
        this.getBukkitPlayer().setScoreboard(cakeScoreboard.getBukkitScoreboard());
    }

    public Scoreboard getBukkitScoreboard() {
        return this.getBukkitPlayer().getScoreboard();
    }

    public void setBukkitScoreboard(Scoreboard scoreboard) {
        this.getBukkitPlayer().setScoreboard(scoreboard);
    }

    public PlayerInventory getInventory() {
        return this.getBukkitPlayer().getInventory();
    }

    public Object getProfile() {
        try {
            Method getProfile = this.getBukkitPlayer().getClass().getMethod("getProfile");
            return getProfile.invoke(this.getBukkitPlayer());
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object getPlayerConnection() {
        try {
            Method getHandle = this.getBukkitPlayer().getClass().getMethod("getHandle");
            Object nmsPlayer = getHandle.invoke(this.getBukkitPlayer());
            Field field = nmsPlayer.getClass().getField("playerConnection");
            return field.get(nmsPlayer);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Location getLocation() {
        return this.getBukkitPlayer().getLocation();
    }

    public void setHelmet(CakeItem cakeItem) {
        this.getInventory().setHelmet(cakeItem);
    }

    public void setChestplate(CakeItem cakeItem) {
        this.getInventory().setChestplate(cakeItem);
    }

    public void setLeggings(CakeItem cakeItem) {
        this.getInventory().setLeggings(cakeItem);
    }

    public void setBoots(CakeItem cakeItem) {
        this.getInventory().setBoots(cakeItem);
    }

    public void sendMessage(String message) {
        this.getBukkitPlayer().sendMessage(message);
    }

    public void sendPacket(Object packet) {
        try {
            this.getPlayerConnection().getClass().getMethod("sendPacket", Utils.getNMSClass("Packet")).invoke(this.getPlayerConnection(), packet);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void sendActionbar(String message) {
        CakeManager.getInstance().sendActionbar(this, message);
    }

    public void sendActionbar(String message, int seconds) {
        CakeManager.getInstance().sendActionbar(this, message, seconds);
    }

    public void sendTitle(String s) {
        Title title = new Title(s);
        title.sendTitle(this);
    }

    public void sendSubtitle(String s) {
        Title title = new Title("", s);
        title.sendTitle(this);
    }

    public void sendTitle(Title title) {
        title.sendTitle(this);
    }

    public boolean hasPermission(String permission) {
        return this.getBukkitPlayer().hasPermission(permission);
    }

    public void teleport(CakePlayer cakePlayer) {
        this.getBukkitPlayer().teleport(cakePlayer.getLocation());
    }

    public void teleport(Entity entity) {
        this.getBukkitPlayer().teleport(entity);
    }

    public void teleport(Location location) {
        this.getBukkitPlayer().teleport(location);
    }

    public void openInventory(CakeInventory inventory) {
        this.getBukkitPlayer().openInventory(inventory);
    }

    public void playSound(Sound sound) {
        this.getBukkitPlayer().playSound(this.getLocation(), sound, 1, 1);
    }

    public void playSound(Sound sound, float volume, float pitch) {
        this.getBukkitPlayer().playSound(this.getLocation(), sound, volume, pitch);
    }

    public void playSound(Location location, Sound sound, float volume, float pitch) {
        this.getBukkitPlayer().playSound(location, sound, volume, pitch);
    }

    public void hidePlayer(Player player) {
        this.getBukkitPlayer().hidePlayer(player);
    }

    public void hidePlayer(CakePlayer cakePlayer) {
        this.getBukkitPlayer().hidePlayer(cakePlayer.getBukkitPlayer());
    }

}
