package me.doktormedrasen.spigot.cakebackend.api;

import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.spigot.cakebackend.utils.Utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
@Setter
public class Title {

    private String title;
    private String subtitle;
    private int fadeIn;
    private int duration;
    private int fadeOut;

    public Title(String title) {
        this(title, "");
    }

    public Title(String title, String subtitle) {
        this(title, subtitle, 100);
    }

    public Title(String title, String subtitle, int duration) {
        this(title, subtitle, 5, duration, 5);
    }

    public Title(String title, String subtitle, int fadeIn, int duration, int fadeOut) {
        this.title = title;
        this.subtitle = subtitle;
        this.fadeIn = fadeIn;
        this.duration = duration;
        this.fadeOut = fadeOut;
    }

    public void sendTitle(CakePlayer player) {
        try {
            Object enumTitle = Utils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null);
            Object titleChat = Utils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\": \"" + this.title + "\"}");

            Object enumSubtitle = Utils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get(null);
            Object subtitleChat = Utils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\": \"" + this.subtitle + "\"}");

            Constructor titleConstructor = Utils.getNMSClass("PacketPlayOutTitle").getConstructor(Utils.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], Utils.getNMSClass("IChatBaseComponent"), int.class, int.class, int.class);
            Object titlePacket = titleConstructor.newInstance(enumTitle, titleChat, this.fadeIn, this.duration, this.fadeOut);
            Object subtitlePacket = titleConstructor.newInstance(enumSubtitle, subtitleChat, this.fadeIn, this.duration, this.fadeOut);

            player.sendPacket(titlePacket);
            player.sendPacket(subtitlePacket);
        } catch (NoSuchMethodException | NoSuchFieldException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
    }

}
