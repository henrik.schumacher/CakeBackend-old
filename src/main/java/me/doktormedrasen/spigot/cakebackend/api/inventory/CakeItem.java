package me.doktormedrasen.spigot.cakebackend.api.inventory;

import com.mojang.authlib.GameProfile;
import me.doktormedrasen.spigot.cakebackend.api.CakePlayer;
import me.doktormedrasen.spigot.cakebackend.api.manager.CakeManager;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class CakeItem extends ItemStack {

    public CakeItem(ItemStack itemStack) {
        super(itemStack);
    }

    public CakeItem(Material material) {
        super(material);
    }

    public CakeItem(Material material, int amount) {
        super(material, amount);
    }

    public CakeItem(Material material, int amount, int type) {
        super(material, amount, (short) type);
    }

    public CakeItem(Material material, String displayname) {
        this(material, 1, 0, displayname);
    }

    public CakeItem(Material material, int amount, String displayname) {
        this(material, amount, 0, displayname);
    }

    public CakeItem(Material material, int amount, int type, String displayname) {
        super(material, amount, (short) type);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(displayname);
        this.setItemMeta(meta);
    }

    public CakeItem setItemMeta1(ItemMeta itemMeta) {
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem setDisplayName(String displayname) {
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.setDisplayName(displayname);
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem setLore(String ... lore) {
        ItemMeta itemMeta = this.getItemMeta();
        List<String> itemLore = Arrays.asList(lore);
        itemMeta.setLore(itemLore);
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem setLore(List<String> lore) {
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.setLore(lore);
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem setSkullTextures(String name) {
        if (!this.getType().equals(Material.SKULL_ITEM)) throw new RuntimeException("Item is not a skull");
        SkullMeta skullMeta = (SkullMeta) this.getItemMeta();
        CakeManager.getInstance().applyTextures(skullMeta, name);
        this.setItemMeta(skullMeta);
        return this;
    }

    public CakeItem setSkullTextures(UUID uuid) {
        if (!this.getType().equals(Material.SKULL_ITEM)) throw new RuntimeException("Item is not a skull");
        SkullMeta skullMeta = (SkullMeta) this.getItemMeta();
        CakeManager.getInstance().applyTextures(skullMeta, uuid);
        this.setItemMeta(skullMeta);
        return this;
    }

    public CakeItem setSkullTextures(CakePlayer player) {
        if (!this.getType().equals(Material.SKULL_ITEM)) throw new RuntimeException("Item is not a skull");
        SkullMeta skullMeta = (SkullMeta) this.getItemMeta();
        CakeManager.getInstance().applyTextures(skullMeta, player);
        this.setItemMeta(skullMeta);
        return this;
    }

    public CakeItem setSkullTextures(GameProfile profile) {
        if (!this.getType().equals(Material.SKULL_ITEM)) throw new RuntimeException("Item is not a skull");
        SkullMeta skullMeta = (SkullMeta) this.getItemMeta();
        CakeManager.getInstance().applyTextures(skullMeta, profile);
        this.setItemMeta(skullMeta);
        return this;
    }

    public CakeItem hideAttributes() {
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_ATTRIBUTES});
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem hidePotionEffects() {
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_POTION_EFFECTS});
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem hideEnchants() {
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_ENCHANTS});
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem setUnbreakable() {
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.spigot().setUnbreakable(true);
        itemMeta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_UNBREAKABLE});
        this.setItemMeta(itemMeta);
        return this;
    }

    public CakeItem addEnchantment1(Enchantment enchantment, int level) {
        this.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    public CakeItem removeEnchantment1(Enchantment enchantment) {
        this.removeEnchantment(enchantment);
        return this;
    }

    public boolean hasEnchant(Enchantment enchantment) {
        return this.getItemMeta().hasEnchant(enchantment);
    }

    public boolean hasEnchants() {
        return this.getItemMeta().hasEnchants();
    }

    public CakeItem setColor(Color color) {
        LeatherArmorMeta meta = (LeatherArmorMeta)this.getItemMeta();
        meta.setColor(color);
        this.setItemMeta(meta);
        return this;
    }

    public boolean equals(ItemStack itemStack) {
        return itemStack.getType().equals(this.getType()) && itemStack.getAmount() == this.getAmount() && itemStack.getDurability() == this.getDurability() && itemStack.getItemMeta() == null == (this.getItemMeta() == null) && (itemStack.getItemMeta() == null || itemStack.getItemMeta().getDisplayName().equals(this.getItemMeta().getDisplayName()));
    }

}
