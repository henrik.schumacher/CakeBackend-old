package me.doktormedrasen.spigot.cakebackend.api.messages;

/**
 * Class by DoktorMedRasen :P
 */

public class EnglishMessages {

    public static String noPermission() {
        return "§cYou §cdo §cnot §chave §cpermission §cto §cperform §cthis §ccommand";
    }

    public static String uuidNotFound(String name) {
        return "§7UUID of §8'§f" + name + "§8' §7could §cnot §7be found";
    }

}
