package me.doktormedrasen.spigot.cakebackend.api.inventory;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

/**
 * Class by DoktorMedRasen :P
 */

public class CakeInventory implements Inventory {

    @Getter
    private Inventory inventory;

    public CakeInventory(int rows, String title) {
        this.inventory = Bukkit.createInventory(null, rows * 9, title);;
    }

    public void setItem(int row, int slot, CakeItem item) {
        this.setItem((row - 1) * 9 + slot - 1, item);
    }

    public CakeItem getItem(int row, int line) {
        return (CakeItem) this.getItem((row - 1) * 9 + line - 1);
    }

    public void addItem(CakeItem item) {
        this.inventory.addItem(item);
    }

    @Override
    public int getSize() {
        return this.inventory.getSize();
    }

    @Override
    public int getMaxStackSize() {
        return this.inventory.getMaxStackSize();
    }

    @Override
    public void setMaxStackSize(int maxStackSize) {
        this.inventory.setMaxStackSize(maxStackSize);
    }

    @Override
    public String getName() {
        return this.inventory.getName();
    }

    @Override
    public ItemStack getItem(int i) {
        return this.inventory.getItem(i);
    }

    @Override
    public void setItem(int i, ItemStack itemStack) {
        this.inventory.setItem(i, itemStack);
    }

    @Override
    public HashMap<Integer, ItemStack> addItem(ItemStack... itemStacks) throws IllegalArgumentException {
        return this.inventory.addItem(itemStacks);
    }

    @Override
    public HashMap<Integer, ItemStack> removeItem(ItemStack... itemStacks) throws IllegalArgumentException {
        return this.inventory.removeItem(itemStacks);
    }

    @Override
    public ItemStack[] getContents() {
        return this.inventory.getContents();
    }

    @Override
    public void setContents(ItemStack[] itemStacks) throws IllegalArgumentException {
        this.inventory.setContents(itemStacks);
    }

    @Deprecated
    public ItemStack[] getStorageContents() {
        return null;
    }

    @Deprecated
    public void setStorageContents(ItemStack[] itemStacks) throws IllegalArgumentException {
    }

    @Override
    @Deprecated
    public boolean contains(int i) {
        return this.inventory.contains(i);
    }

    @Override
    public boolean contains(Material material) throws IllegalArgumentException {
        return this.inventory.contains(material);
    }

    @Override
    public boolean contains(ItemStack itemStack) {
        return this.inventory.contains(itemStack);
    }

    @Override
    @Deprecated
    public boolean contains(int i, int i1) {
        return this.inventory.contains(i, i1);
    }

    @Override
    public boolean contains(Material material, int i) throws IllegalArgumentException {
        return this.inventory.contains(material, i);
    }

    @Override
    public boolean contains(ItemStack itemStack, int i) {
        return this.inventory.contains(itemStack, i);
    }

    @Override
    public boolean containsAtLeast(ItemStack itemStack, int i) {
        return this.inventory.containsAtLeast(itemStack, i);
    }

    @Override
    @Deprecated
    public HashMap<Integer, ? extends ItemStack> all(int i) {
        return this.inventory.all(i);
    }

    @Override
    public HashMap<Integer, ? extends ItemStack> all(Material material) throws IllegalArgumentException {
        return this.inventory.all(material);
    }

    @Override
    public HashMap<Integer, ? extends ItemStack> all(ItemStack itemStack) {
        return this.inventory.all(itemStack);
    }

    @Override
    @Deprecated
    public int first(int i) {
        return this.inventory.first(i);
    }

    @Override
    public int first(Material material) throws IllegalArgumentException {
        return this.inventory.first(material);
    }

    @Override
    public int first(ItemStack itemStack) {
        return this.inventory.first(itemStack);
    }

    @Override
    public int firstEmpty() {
        return this.inventory.firstEmpty();
    }

    @Override
    @Deprecated
    public void remove(int i) {
        this.inventory.remove(i);
    }

    @Override
    public void remove(Material material) throws IllegalArgumentException {
        this.inventory.remove(material);
    }

    @Override
    public void remove(ItemStack itemStack) {
        this.inventory.remove(itemStack);
    }

    @Override
    public void clear(int i) {
        this.inventory.clear(i);
    }

    @Override
    public void clear() {
        this.inventory.clear();
    }

    @Override
    public List<HumanEntity> getViewers() {
        return this.inventory.getViewers();
    }

    @Override
    public String getTitle() {
        return this.inventory.getTitle();
    }

    @Override
    public InventoryType getType() {
        return  this.inventory.getType();
    }

    @Override
    public InventoryHolder getHolder() {
        return  this.inventory.getHolder();
    }

    @Override
    public ListIterator<ItemStack> iterator() {
        return  this.inventory.iterator();
    }

    @Override
    public ListIterator<ItemStack> iterator(int i) {
        return  this.inventory.iterator(i);
    }

    @Deprecated
    public Location getLocation() {
        return null;
    }

}
