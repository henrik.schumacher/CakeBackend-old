package me.doktormedrasen.spigot.cakebackend.api.scoreboard;

import lombok.Getter;
import me.doktormedrasen.spigot.cakebackend.api.CakePlayer;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class CakeScoreboard {

    private CakePlayer player;
    private Scoreboard bukkitScoreboard;

    public CakeScoreboard(CakePlayer player) {
        this.player = player;
        this.bukkitScoreboard = player.getBukkitPlayer().getScoreboard();
        player.getBukkitPlayer().setScoreboard(this.bukkitScoreboard);
    }

    public CakeScoreboardObjective getObjective() {
        return new CakeScoreboardObjective(this);
    }

    public void registerScoreboardTeam(String registration) {
        if (registration.length() > 16) {
            registration = registration.substring(0, 16);
        }
        Team team = this.bukkitScoreboard.getTeam(registration);
        if (team == null) {
            team = this.bukkitScoreboard.registerNewTeam(registration);
        }
    }

    public void unregisterScoreboardTeam(String registration) {
        Team team = this.bukkitScoreboard.getTeam(registration);
        if (team != null) team.unregister();
    }

    public void registerPlayer(CakePlayer player, String registration, String prefix) {
        if (registration.length() > 16) {
            registration = registration.substring(0, 16);
        }
        if (prefix.length() > 16) {
            prefix = prefix.substring(0, 16);
        }
        Team team = this.bukkitScoreboard.getTeam(registration);
        if (team == null) {
            team = this.bukkitScoreboard.registerNewTeam(registration);
        }
        team.setPrefix(prefix);
        team.setCanSeeFriendlyInvisibles(false);
        team.addEntry(player.getName());
    }

    public void unregisterPlayer(CakePlayer player) {
        String registration = "";
        if (registration.length() > 16) {
            registration = registration.substring(0, 16);
        }
        this.unregisterScoreboardTeam(registration);
    }

}
