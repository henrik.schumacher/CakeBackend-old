package me.doktormedrasen.spigot.cakebackend.api.scoreboard;

import lombok.Getter;
import me.doktormedrasen.spigot.cakebackend.api.manager.CakeManager;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.Map;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class CakeScoreboardObjective {

    private CakeScoreboard scoreboard;
    private Objective bukkitObjective;

    public CakeScoreboardObjective(CakeScoreboard scoreboard) {
        this.scoreboard = scoreboard;
        this.bukkitObjective = scoreboard.getBukkitScoreboard().getObjective(DisplaySlot.SIDEBAR);
    }

    public CakeScoreboardScore getScore(int score) {
        Scoreboard board = scoreboard.getBukkitScoreboard();
        Map<Integer, String> map = new HashMap();

        for(String entry : board.getEntries()) {
            map.put(bukkitObjective.getScore(entry).getScore(), entry);
        }
        return new CakeScoreboardScore(this, map.get(score), score);
    }

    public void setScores(String[] scores) {
        Scoreboard board = this.scoreboard.getBukkitScoreboard();
        if (board == null) {
            board = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        int number = scores.length;

        for (Objective obj : board.getObjectives()) {
            if (obj.getDisplaySlot() == DisplaySlot.SIDEBAR) {
                obj.unregister();
            }
        }

        Objective obj = board.registerNewObjective("aaa", "bbb");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(CakeManager.getInstance().getScoreboardDisplayName());


        for (String score : scores) {
            obj.getScore(score).setScore(number);
            --number;
        }
    }

}
