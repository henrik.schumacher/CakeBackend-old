package me.doktormedrasen.spigot.cakebackend.api.scoreboard;

import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

/**
 * Class by DoktorMedRasen :P
 */

public class CakeScoreboardScore {

    private CakeScoreboardObjective objective;
    private Objective bukkitObjective;
    private String name;
    private int score;

    public CakeScoreboardScore(CakeScoreboardObjective objective, String name, int score) {
        this.objective = objective;
        this.bukkitObjective = objective.getBukkitObjective();
        this.name = name;
        this.score = score;
    }

    public void setName(String name) {
        Scoreboard scoreboard = this.bukkitObjective.getScoreboard();
        Objective objective = scoreboard.getObjective(DisplaySlot.SIDEBAR);

        scoreboard.resetScores(this.name);
        objective.getScore(name).setScore(this.score);
    }

}
