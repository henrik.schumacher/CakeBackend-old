package me.doktormedrasen.spigot.cakebackend.api;

import lombok.Getter;
import org.bukkit.Bukkit;

import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class PlayerData {

    private PlayerData player;
    private UUID uniqueId;
    private String name;

    public PlayerData(UUID uniqueId) {
        this.player = this;
        this.uniqueId = uniqueId;
        this.name = Bukkit.getOfflinePlayer(uniqueId).getName();
    }

    public boolean isOnline() {
        return Bukkit.getOfflinePlayer(this.uniqueId).isOnline();
    }

}
