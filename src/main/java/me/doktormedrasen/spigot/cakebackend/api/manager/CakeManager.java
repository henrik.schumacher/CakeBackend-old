package me.doktormedrasen.spigot.cakebackend.api.manager;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.spigot.cakebackend.Backend;
import me.doktormedrasen.spigot.cakebackend.api.CakePlayer;
import me.doktormedrasen.spigot.cakebackend.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class CakeManager {

    @Getter
    private static CakeManager instance;
    @Setter
    private String scoreboardDisplayName;
    private Map<String, Integer> actionbar = new HashMap<>();

    public CakeManager() {
        instance = this;
        this.scoreboardDisplayName = "§c§lCAKE";
    }

    public void disable() {
        instance = null;
    }

    public CakePlayer getPlayer(Player player) {
        return new CakePlayer(player.getUniqueId());
    }

    public CakePlayer getPlayer(UUID uniqueId) {
        return new CakePlayer(uniqueId);
    }

    public CakePlayer getPlayer(String name) {
        return new CakePlayer(Bukkit.getOfflinePlayer(name).getUniqueId());
    }

    public SkullMeta applyTextures1(SkullMeta meta, String texture) {
        return this.applyTextures(meta, Utils.buildProfile(texture));
    }

    public SkullMeta applyTextures(SkullMeta meta, String name) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", Utils.getPlayerTextures(name)));
        return this.applyTextures(meta, profile);
    }

    public SkullMeta applyTextures(SkullMeta meta, UUID uuid) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", Utils.getPlayerTextures(uuid)));
        return this.applyTextures(meta, profile);
    }

    public SkullMeta applyTextures(SkullMeta meta, Player player) {
        return this.applyTextures(meta, (GameProfile) this.getPlayer(player).getProfile());
    }

    public SkullMeta applyTextures(SkullMeta meta, CakePlayer player) {
        return this.applyTextures(meta, (GameProfile) player.getProfile());
    }

    public SkullMeta applyTextures(SkullMeta meta, GameProfile profile) {
        try {
            Field field = meta.getClass().getDeclaredField("profile"); //TODO: VERSION!
            field.setAccessible(true);
            field.set(meta, profile);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return meta;
    }

    /* TODO: Reflections
    public void applyTextures(Skull skull, String name) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", Utils.getPlayerTextures(name)));
        this.applyTextures(skull, profile);
    }

    public void applyTextures(Skull skull, UUID uuid) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", Utils.getPlayerTextures(uuid)));
        this.applyTextures(skull, profile);
    }

    public void applyTextures(Skull skull, Player player) {
        this.applyTextures(skull, (GameProfile) this.getPlayer(player).getProfile());
    }

    public void applyTextures(Skull skull, CakePlayer player) {
        this.applyTextures(skull, (GameProfile) player.getProfile());
    }

    public void applyTextures(Skull skull, GameProfile profile) {
        Location location = skull.getLocation();
        WorldServer server = ((CraftWorld)location.getWorld()).getHandle();
        TileEntity entity = server.getTileEntity(new BlockPosition(skull.getX(), skull.getY(), skull.getZ()));

        try {
            Field field = TileEntitySkull.class.getDeclaredField("g");
            field.setAccessible(true);
            field.set(entity, profile);
            server.notify(new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void sendActionbar(CakePlayer player, String message) {
        try {
            Object component = Utils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\": \"" + message + "\"}");
            Constructor constructor = Utils.getNMSClass("PacketPlayOutChat").getConstructor(Utils.getNMSClass("IChatBaseComponent"), byte.class);

            final int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Backend.getInstance(), () -> {
                try {
                    player.sendPacket(constructor.newInstance(component, (byte) 2));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, 0, 20);
            actionbar.put(player.getName(), task);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendActionbar(CakePlayer player, String message, int seconds) {
        try {
            Object component1 = Utils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\": \"" + message + "\"}");
            Constructor constructor = Utils.getNMSClass("PacketPlayOutChat").getConstructor(Utils.getNMSClass("IChatBaseComponent"), byte.class);

            final int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Backend.getInstance(), new Runnable() {
                int time = seconds;
                public void run() {
                    time--;

                    if (time > 0) {
                        try {
                            player.sendPacket(constructor.newInstance(component1, (byte) 2));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, 0, 20);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopActionbar(CakePlayer player) {
        if (actionbar.containsKey(player.getName())) {
            Bukkit.getScheduler().cancelTask(actionbar.get(player.getName()));
            actionbar.remove(player.getName());
        }
    }

}
