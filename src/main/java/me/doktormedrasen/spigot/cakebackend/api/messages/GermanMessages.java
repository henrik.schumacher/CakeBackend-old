package me.doktormedrasen.spigot.cakebackend.api.messages;

/**
 * Class by DoktorMedRasen :P
 */

public class GermanMessages {

    public static String noPermission() {
        return "§cDu hast keine Berechtigung um diesen Befehl auszuführen";
    }

    public static String uuidNotFound(String name) {
        return "§7UUID von §8'§f" + name + "§8' §7konnte §cnicht §7gefunden werden";
    }

}
